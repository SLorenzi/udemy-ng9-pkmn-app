import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Pokemon } from '../pokemon';
import { PokemonsService } from '../pokemons.service';

@Component({
  moduleId: module.id
  , selector: 'detail-pokemon'
  , templateUrl: 'detail-pokemon.component.html'
})
export class DetailPokemonComponent implements OnInit {

  private pokemon: Pokemon = null;

  constructor(
    private route: ActivatedRoute
    , private router: Router
    , private titleService: Title
    , private pokemonsService: PokemonsService
  ) { }

  ngOnInit(): void {
    let id = +this.route.snapshot.paramMap.get('id');
    this.pokemonsService.getPokemon(id)
      .subscribe(pokemon => {
        this.pokemon = pokemon;
        this.titleService.setTitle(`Détails de ${pokemon.name}`);
      });
  }

  delete(pokemon: Pokemon): void {
    this.pokemonsService.deletePokemon(pokemon)
      .subscribe(_ => this.goBack());
  }

  goBack(): void {
    this.router.navigate(['/pokemon/all']);
  }

  goEdit(pokemon: Pokemon): void {
    this.router.navigate(['/pokemon/edit', pokemon.id]);
  }
}
