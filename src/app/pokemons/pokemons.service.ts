import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Pokemon } from "./pokemon";

@Injectable()
export class PokemonsService {

  private pokemonsUrl = 'api/pokemons';

  constructor(private http: HttpClient) { }

  private log(log: string) {
    console.info(log);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`)
      return of(result as T);
    }
  }

  getPokemons(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(this.pokemonsUrl).pipe(
      tap(_ => this.log('fetched pokemons'))
      , catchError(this.handleError<Pokemon[]>('getpokemons', []))
    );
  }

  getPokemon(id: number): Observable<Pokemon> {
    const url = `${this.pokemonsUrl}/${id}`;
    return this.http.get<Pokemon>(url).pipe(
      tap(_ => this.log(`fetched pokemon id=${id}`))
      , catchError(this.handleError<Pokemon>(`getPokemon id=${id}`))
    );
  }

  searchPokemons(term: string): Observable<Pokemon[]> {
    if (!term.trim()) {
      return of([]);
    }

    const url = `${this.pokemonsUrl}/?name=${term}`

    return this.http.get<Pokemon[]>(url).pipe(
      tap(_ => this.log(`found pokemons matching ${term}`))
      , catchError(this.handleError<Pokemon[]>(`searchPokemons term=${term}`, []))
    );
  }

  updatePokemon(pokemon: Pokemon): Observable<Pokemon> {
    const url = `${this.pokemonsUrl}/${pokemon.id}`;
    const httpOptions = {
      headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    return this.http.put(url, pokemon, httpOptions).pipe(
      tap(_ => this.log(`updated pokemon id=${pokemon.id}`))
      , catchError(this.handleError<any>(`updatePokemon id=${pokemon.id}`))
    );
  }

  deletePokemon(pokemon: Pokemon): Observable<Pokemon> {
    const url = `${this.pokemonsUrl}/${pokemon.id}`;

    return this.http.delete<Pokemon>(url).pipe(
      tap(_ => this.log(`deleted pokemon id=${pokemon.id}`))
      , catchError(this.handleError<Pokemon>(`deletePokemon id=${pokemon.id}`))
    );
  }

  getPokemonTypes(): string[] {
    return [
      'Feu', 'Eau', 'Plante', 'Insecte', 'Normal', 'Vol'
      , 'Poison', 'Fée', 'Psy', 'Electrik', 'Combat'
    ];
  }
}
