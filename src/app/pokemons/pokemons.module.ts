import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LoaderComponent } from '../loader.component'

import { PokemonRoutingModule } from './pokemons-routing.module';
import { PokemonsService } from './pokemons.service';
import { ListPokemonComponent } from './list/list-pokemon.component';
import { DetailPokemonComponent } from './detail/detail-pokemon.component';
import { EditPokemonComponent } from './edit/edit-pokemon.component';
import { PokemonFormComponent } from './edit/pokemon-form.component';
import { PokemonSearchComponent } from './search/search-pokemon.component';
import { BorderCardDirective } from './border-card.directive';
import { PokemonTypeColorPipe } from './pokemon-type-color.pipe';

@NgModule({
  imports: [
    CommonModule
    , FormsModule
    , PokemonRoutingModule
  ]
  , declarations: [
    LoaderComponent
    , ListPokemonComponent
    , DetailPokemonComponent
    , EditPokemonComponent
    , PokemonFormComponent
    , PokemonSearchComponent
    , BorderCardDirective
    , PokemonTypeColorPipe
  ]
  , providers: [ PokemonsService ]
})
export class PokemonsModule { }
