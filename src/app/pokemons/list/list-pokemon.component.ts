import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Pokemon } from '../pokemon';
import { PokemonsService } from '../pokemons.service';

@Component({
  moduleId: module.id
  , selector: 'list-pokemon'
  , templateUrl: 'list-pokemon.component.html'
})
export class ListPokemonComponent implements OnInit {

  private pokemons : Pokemon[];
  private title: string = 'Liste des Pokémons';

  constructor(
    private router: Router
    , private titleService: Title
    , private pokemonsService: PokemonsService
  ) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.getPokemons();
  }

  getPokemons(): void {
    this.pokemonsService.getPokemons()
      .subscribe(pokemons => this.pokemons = pokemons);
  }

  selectPokemon(pokemon: Pokemon) {
    console.log(`Clicked on ${pokemon.name}`);
    let link = ['/pokemon', pokemon.id];
    this.router.navigate(link);
  }
}
