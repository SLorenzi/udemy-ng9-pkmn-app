import { Pipe, PipeTransform } from '@angular/core';

/*
 * Affiche la couleur correspondant au type du pokémon.
 * Prend en argument le type du pokémon.
 * Exemple d'utilisation:
 *   {{ pokemon.type | pokemonTypeColor }}
*/
@Pipe({name: 'pokemonTypeColor'})
export class PokemonTypeColorPipe implements PipeTransform {

  private readonly COLORS = {
    'Feu': 'red lighten-1'
    , 'Eau': 'blue lighten-1'
    , 'Plante': 'green lighten-1'
    , 'Insecte': 'brown lighten-1'
    , 'Normal': 'grey lighten-3'
    , 'Vol': 'blue lighten-3'
    , 'Poison': 'deep-purple accent-1'
    , 'Fée': 'pink lighten-4'
    , 'Psy': 'deep-purple darken-2'
    , 'Electrik': 'lime accent-1'
    , 'Combat': 'deep-orange'
  };

  transform(type: string): string {
    return 'chip ' + (this.COLORS[type] || 'grey');
  }
}
