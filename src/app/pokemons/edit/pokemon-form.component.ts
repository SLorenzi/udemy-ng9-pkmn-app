import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PokemonsService } from '../pokemons.service';
import { Pokemon } from '../pokemon';

@Component({
  moduleId: module.id
  , selector: 'pokemon-form'
  , templateUrl: 'pokemon-form.component.html'
  , styleUrls: ['pokemon-form.component.css']
})
export class PokemonFormComponent implements OnInit {

  @Input() pokemon: Pokemon; // propriété d'entrée du composant
  types: Array<string>; // types disponibles pour un pokémon : 'Eau', 'Feu', etc ...

  constructor(
    private pokemonsService: PokemonsService,
    private router: Router
  ) { }

  ngOnInit() {
    // Initialisation de la propriété types
    this.types = this.pokemonsService.getPokemonTypes();
  }

  // Détermine si le type passé en paramètres appartient ou non au pokémon en cours d'édition.
  hasType(type: string): boolean {
    return this.pokemon.types.includes(type);
  }

  // Méthode appelée lorsque l'utilisateur ajoute ou retire un type au pokémon en cours d'édition.
  selectType($event: any, type: string): void {
    let checked = $event.target.checked;
    if (checked) {
      this.pokemon.types.push(type);
    } else {
      let index = this.pokemon.types.indexOf(type);
      if (index > -1) {
        this.pokemon.types.splice(index, 1);
      }
    }
  }

  // Indique si on peut ajouter/enlever un type au pokemon
  isTypeAllowed(type: string): boolean {
    return (!this.hasType(type) && this.pokemon.types.length < 3) // pas plus de 3 types
      || (this.hasType(type) && this.pokemon.types.length > 1); // pas moins d'un type
  }

  // La méthode appelée lorsque le formulaire est soumis.
  onSubmit(): void {
    console.log("Submit form !");
    this.pokemonsService.updatePokemon(this.pokemon)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    let link = ['/pokemon', this.pokemon.id];
    this.router.navigate(link);
  }
}
