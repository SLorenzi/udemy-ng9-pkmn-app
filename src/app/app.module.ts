import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { AppRoutingModule } from './app-routing.module';
import { LoginModule } from './login/login.module'
import { PokemonsModule } from './pokemons/pokemons.module';
import { AuthGuard } from './auth-guard.service';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found.component';

@NgModule({
  imports: [
    // ATTENTION l'ordre est important -> cf. declaration des routes
    BrowserModule
    , HttpClientModule
    , HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {dataEncapsulation: false})
    , LoginModule
    , PokemonsModule
    , AppRoutingModule
  ]
  , declarations: [
    AppComponent
    , PageNotFoundComponent
  ]
  , providers: [ Title, AuthGuard ]
  , bootstrap: [ AppComponent ]
})
export class AppModule { }
